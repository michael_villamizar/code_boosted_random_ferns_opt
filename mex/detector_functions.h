#ifndef detector_functions_h
#define detector_functions_h

#include "detector_functions.h"

using namespace std;

// object detection
cv::Mat fun_detection(cv::Mat prms, cv::Mat img, cv::Mat WCs, cv::Mat hstms, cv::Mat ferns, cv::Mat objSize, int fernSize, double detThr);

// get detections hypotheses from detection data
void fun_detection_hypotheses(cv::Mat detData, vector<cv::Rect>& vecBoxes, vector<double>& vecScores, cv::Mat objSize, double scaleRatio, double detThr, int border);

// test classifier using sliding window
cv::Mat fun_test_classifier(cv::Mat fernsMaps, cv::Mat WCs, cv::Mat hstms, cv::Mat objSize, int numHypotheses);

// ferns output maps
cv::Mat fun_ferns_maps(cv::Mat HOG, cv::Mat ferns, int fernSize);

// histogram of oriented gradients (HOG) from integral image (II)
cv::Mat fun_II2HOG(cv::Mat II, int cellSize);

// integral image (II) from histogram of oriented gradients (HOG)
cv::Mat fun_HOG2II(cv::Mat HOG);

// histogram of oriented gradients (HOG)
cv::Mat fun_HOG(cv::Mat img, int numChannels, int cellSize);

#endif
