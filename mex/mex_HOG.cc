#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include "mexopencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include "detector_functions.h"
#include "detector_functions.cpp"

using namespace std;

// main function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  // check
  if(nrhs!=3) { mexErrMsgTxt("Three inputs required: 1. image 2. num. gradient channels 3. cell size"); }
  if(nlhs!=1) { mexErrMsgTxt("One output required: 1. HOG"); }

  // variables
  cv::Mat dimg;  // double image
  cv::Mat img = MxArray(prhs[0]).toMat();  // input image
  int numChannels = MxArray(prhs[1]).toInt();  // HOG: num. gradient orientations -channels-
  int cellSize = MxArray(prhs[2]).toInt();  // HOG: cell size

  // check unint8 image
  if (img.type()!=0 && img.type()!=16){mexErrMsgTxt("The input image must be unint8");}

  // convert to gray-scale image
  if (img.channels()==3){cv::cvtColor(img, img, CV_BGR2GRAY);}

  // convert to double image
  img.convertTo(dimg, CV_64FC1);

  // compute histogram of oriented gradients (HOG)
  cv::Mat HOG = fun_HOG(dimg, numChannels, cellSize);

  // output
  plhs[0] = MxArray(HOG);
}
