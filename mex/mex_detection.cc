#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include "mexopencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include "detector_functions.h"
#include "detector_functions.cpp"

// variables
#define PI 3.14159265

using namespace std;

// main function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  // check
  if(nrhs!=8) { mexErrMsgTxt("Eight inputs required"); }
  if(nlhs!=1) { mexErrMsgTxt("One output required"); }

  // variables
  cv::Mat dimg;  // double image
  cv::Mat prms = MxArray(prhs[0]).toMat();  // detection parameters
  cv::Mat img = MxArray(prhs[1]).toMat();  // input image
  cv::Mat WCs = MxArray(prhs[2]).toMat();  // classifier: weak classifiers data
  cv::Mat hstms = MxArray(prhs[3]).toMat();  // classifier: weak classifiers probabilities
  cv::Mat ferns = MxArray(prhs[4]).toMat(CV_64F, false);  // classifier: random ferns data
  cv::Mat objSize = MxArray(prhs[5]).toMat();  // classifier: object size
  int fernSize = MxArray(prhs[6]).toInt();  // classifier: fern size
  double detThr = MxArray(prhs[7]).toDouble();  // detection threshold

  // check unint8 image
  if (img.type()!=0 && img.type()!=16){mexErrMsgTxt("The input image must be uint8");}

  // convert to gray-scale image
  if (img.channels()==3){cv::cvtColor(img, img, CV_BGR2GRAY);}

  // convert to double image
  img.convertTo(dimg, CV_64FC1); 

  // object detection
  cv::Mat output = fun_detection(prms, dimg, WCs, hstms, ferns, objSize, fernSize, detThr);

  // output: detections
  plhs[0] = MxArray(output);
}
