#include <string>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

#define PI 3.14159265
using namespace std;

// object detection
cv::Mat fun_detection(cv::Mat prms, cv::Mat inImg, cv::Mat WCs, cv::Mat hstms, cv::Mat ferns, cv::Mat objSize, int fernSize, double detThr){

  // check parameters
  if (prms.cols!=8) { mexErrMsgTxt("Incorrect number of input detection parameters"); }

  // detection  parameters
  int imgHeight = prms.at<double>(0,0);  // standard image height
  int numHypotheses = prms.at<double>(0,1);  // num object hypotheses
  int minImgSize = prms.at<double>(0,2);  // min. image size
  int numChannels = prms.at<double>(0,3);  // num. gradient orientation bins -image feature channels-
  int minCellSize = prms.at<double>(0,4);  // min. cell size
  int maxCellSize = prms.at<double>(0,5);  // max. cell size
  int numImgLevels = prms.at<double>(0,6);  // num. image levels -image pyramid-
  int imgExt = prms.at<double>(0,7);  // image extension (padding)
  int objSY = (int)objSize.at<double>(0,0);  // object size y
  int objSX = (int)objSize.at<double>(0,1);  // object size x

  // variables
  int tmp;  // temp. variable
  cv::Mat II;  // integral image
  int cSX,cSY;  // current image size
  double score;  // detection score
  cv::Rect box;  // detection box
  cv::Mat detData;  // detection results data
  cv::Mat HOG,cHOG;  // histograms of oriented gradients
  cv::Mat cImg,img;  // images
  cv::Mat fernsMaps;  // fern output maps
  int sy = inImg.rows;  // image size y
  int sx = inImg.cols;  // image size x
  int iterDet,numDets;  // detections
  int numWCs = WCs.rows;  // num. ferns
  int numBins = hstms.cols;  // num. histogram bins
  int numFeats = ferns.cols;  // num. binary features
  int numFerns = ferns.channels();  // num. random ferns
  vector<double> scores;   // detection scores
  vector<cv::Rect> boxes;   // detection boxes
  int numScales,numOctaves;  // num. image scales and octaves
  double scaRatio,scaFact,rszFact;  // scale ratio and factor

  // image padding
  cv::copyMakeBorder(inImg, img, imgExt, imgExt, imgExt, imgExt, cv::BORDER_REPLICATE);

  // image resize factor
  rszFact = (double)img.rows/imgHeight;

  // image resize -standard height-
  cv::resize(img, img, cvSize(round((double)img.cols/rszFact), round((double)img.rows/rszFact)));

  // num. octaves
  numOctaves = floor(log2(min(img.cols,img.rows))) - log2(minImgSize);

  // num. scales
  numScales = numOctaves*numImgLevels;

  // image scales
  for (int iterScale=0; iterScale<numScales; iterScale++){

    // image size
    scaRatio = pow(2,((double)iterScale/numImgLevels));  // scale ratio
    cSY = round((double)img.rows/scaRatio);  // new image size y
    cSX = round((double)img.cols/scaRatio);  // new image size x

    // image resize
    cv::resize(img, cImg, cvSize(cSX, cSY));

    // HOG computation
    HOG = fun_HOG(cImg, numChannels, 1);

    // integral image (II)
    II = fun_HOG2II(HOG);

    // image pyramid
    for (int iterCell=minCellSize; iterCell<=maxCellSize; iterCell++){

      // scale factor
      scaFact = iterCell*scaRatio*rszFact;

      // current scaled HOG
      cHOG = fun_II2HOG(II, iterCell);

      // check size
      if ((cHOG.rows-objSY)<=0 || (cHOG.cols-objSX)<=0){ break; }

      // convolve random ferns on the current HOG
      fernsMaps = fun_ferns_maps(cHOG, ferns, fernSize);

      // test the classifier using sliding window
      detData = fun_test_classifier(fernsMaps, WCs, hstms, objSize, numHypotheses);

      // detection hypotheses
      fun_detection_hypotheses(detData, boxes, scores, objSize, scaFact, detThr, imgExt);

    }
  }

  // num. detections
  numDets = (int)boxes.size();

  // output detection data
  cv::Mat dets = cv::Mat(numDets, 5, CV_64FC1, cv::Scalar::all(0));
  double* detsPtr = dets.ptr<double>(0);

  // detections
  for (iterDet=0; iterDet<numDets; iterDet++){
   // detection score and box
   score = scores.at(iterDet);
   box = boxes.at(iterDet);
   // save detection
   tmp = iterDet*5;
   detsPtr[tmp + 0] = score;
   detsPtr[tmp + 1] = box.x;  // x1
   detsPtr[tmp + 2] = box.y;  // y1
   detsPtr[tmp + 3] = box.x + box.width;  // x2
   detsPtr[tmp + 4] = box.y + box.height;  // y2
  }

  // output
  return dets;
}

// get detections hypotheses from detection data
void fun_detection_hypotheses(cv::Mat detData, vector<cv::Rect>& vecBoxes, vector<double>& vecScores, cv::Mat objSize, double scaleRatio, double detThr, int imgExt){

  // variables
  int w,h;  // box size
  int x,y;  // box coordinates
  cv::Rect box;  // detection box
  double score;  // detection score
  int numDets = detData.rows;  // num. detections
  int oy = (int)objSize.at<double>(0,0);  // object size y
  int ox = (int)objSize.at<double>(0,1);  // object size x

  // temp. variables
  int tmp;

  // pointer
  double* detDataPtr = detData.ptr<double>(0);

  // detections
  for (int itr=0; itr<numDets; itr++){

    // temp. variable
    tmp = itr*3;

    // detection score
    //score = *(detDataPtr + tmp + 0);
    score = *(detDataPtr + tmp);

    // check score
    if (score<detThr){ continue; }

    // detection bounding box
    x = round(detDataPtr[tmp + 1] * scaleRatio) - imgExt;
    y = round(detDataPtr[tmp + 2] * scaleRatio) - imgExt;

    // check empty detection
    if (x==0 && y==0){ continue; }

    // object width and height
    w = ox*scaleRatio;
    h = oy*scaleRatio;

    // detection box
    box = cv::Rect(x, y, w, h);

    // save values
    vecScores.push_back(score);
    vecBoxes.push_back(box);

  }
}

// test classifier using sliding window
cv::Mat fun_test_classifier(cv::Mat fernsMaps, cv::Mat WCs, cv::Mat hstms, cv::Mat objSize, int numHypotheses){

  // parameters
  int minWCs = 50;  // naive cascade: min. num. weak classifiers
  double minThr = -0.1;  // naive cascade: min. detection threshold 

  // variables
  int z;  // fern output
  int index;  // index
  double score;  // detection score
  int fy,fx,fi;  // weak classifier data
  //int sy = fernsMaps.rows;  // map size y
  //int sx = fernsMaps.cols;  // map size x
  int sy = fernsMaps.cols;  // map size y
  int sx = fernsMaps.rows;  // map size x
  int oy = (int)objSize.at<double>(0,0);  // object size y
  int ox = (int)objSize.at<double>(0,1);  // object size x
  int my = sy - oy;  // max. scanning size y
  int mx = sx - ox;  // max. scanning size x
  int numWCs = WCs.rows;  // num. weak classifiers
  int numBins = hstms.cols;  // num. histogram bins
  int numFerns = fernsMaps.channels();  // num. random ferns

  // temp. variables
  int tmp1,tmp3,tmp4,tmp5;
  int tmp2 = sx*numFerns;
  int tmp6 = numHypotheses-1;

  // output detection data
  cv::Mat detData = cv::Mat(numHypotheses, 3, CV_64FC1, cv::Scalar::all(0));

  // detection map
  cv::Mat detMap = cv::Mat(sy, sx, CV_64FC1, cv::Scalar::all(0));

  // pointers
  double* WCsPtr = WCs.ptr<double>(0);
  double* hstmsPtr = hstms.ptr<double>(0);
  double* detMapPtr = detMap.ptr<double>(0);
  double* detDataPtr = detData.ptr<double>(0);
  double* fernsMapsPtr = fernsMaps.ptr<double>(0);

  // scanning
  for (int y=0; y<=my; y++){
    for (int x=0; x<=mx; x++){

      // detection score
      score = 0;

      // test each weak classifier
      for (int wc=0; wc<numWCs; wc++){

        //++++++++++++++++++++++++++++++++++++
        // Mike: change matlab format +1
        //++++++++++++++++++++++++++++++++++++

        // weak classifier data: fern location and index
        tmp1 = wc*3;  // temp. variable
        fy = y + (int)WCsPtr[tmp1] - 1;
        fx = x + (int)WCsPtr[tmp1 + 1] - 1;
        fi =     (int)WCsPtr[tmp1 + 2] - 1;

        // check
        //if (fy<0 || fy>=sy || fx<0 || fx>=sx){ mexErrMsgTxt("Incorrect fern location"); }

        //++++++++++++++++++++++++++++++++++++
        // Mike: change matlab format +1
        //++++++++++++++++++++++++++++++++++++

        // fern output associated with the current weak classifier
        //z = (int)fernsMapsPtr[fy*tmp2 + fx*numFerns + fi] - 1;
        //z = (int)fernsMapsPtr[fx*fernsMaps.cols*numFerns + fy*numFerns + fi] - 1;
        z = (int)fernsMapsPtr[fx*fernsMaps.cols*numFerns + fy*numFerns + fi] - 1;

        // check fern output
        //if (z<0 || z>=numBins){ mexErrMsgTxt("Incorrect fern output"); }

        // update detection score
        score +=  hstmsPtr[wc*numBins + z];

        // naive cascade
        if (wc>minWCs){
          if (score<=minThr*(double)wc){ break; }
        }

      }

      // detection map
      detMapPtr[y*sx + x] = (double)score/numWCs;
    }
  }

  // max. detection hypotheses
  for (int y=0; y<sy; y++){
    for (int x=0; x<sx; x++){

      // detection score
      score = detMapPtr[y*sx + x];

      // check
      if (score<minThr){ continue; }

      // comparing with previous values
      index = -1;
      for (int itr=tmp6; itr>=0; itr--){
        if (score>detDataPtr[itr*3 + 0]){ index = itr; }
      }

      // check
      if (index==-1){ continue; }

      // move previous results
      for (int itr=tmp6; itr>index; itr--){
        tmp3 = itr*3;  // temp. variable
        tmp4 = (itr-1)*3;  // temp. variable
        detDataPtr[tmp3 + 0] = detDataPtr[tmp4 + 0];
        detDataPtr[tmp3 + 1] = detDataPtr[tmp4 + 1];
        detDataPtr[tmp3 + 2] = detDataPtr[tmp4 + 2];
      }

      // save detection data: location and score
      tmp5 = index*3;  // temp. variable
      detDataPtr[tmp5 + 0] = score;
      detDataPtr[tmp5 + 1] = x + 1;
      detDataPtr[tmp5 + 2] = y + 1;

    }
  }

  // output
  return detData;
}

// ferns output maps
cv::Mat fun_ferns_maps(cv::Mat HOG, cv::Mat ferns, int fernSize){

  // variables
  int z;  // fern  output
  int x,y;  // image location iterations
  int r,f;  // fern and feature iterations
  double va,vb;  // points values
  int sy = HOG.rows;  // HOG height
  int sx = HOG.cols;  // HOG width
  int my = sy-fernSize;  // max. size y
  int mx = sx-fernSize;  // max. size x
  int xa,ya,ca,xb,yb,cb;  // HOG locations
  int nc = HOG.channels();  // num. HOG channels
  int numFeats = ferns.cols;  // num. binary features
  int numFerns = ferns.channels();  // num. random ferns
  int numBins = (int)pow(2,numFeats);   // num. fern outputs : histogram bins

  // temp. variables
  int tmp1 = numFeats*numFerns;
  int tmp3 = sx*nc;
  //int tmp4 = sx*numFerns;
  int tmp4 = sy*numFerns; // to transpose the output
  int tmp5 = 2*tmp1;
  int tmp6 = 3*tmp1;
  int tmp7 = 4*tmp1;
  int tmp8 = 5*tmp1;
  int tmp2,tmp9,tmp10,tmp11,tmp12;

  // fern output maps
  //cv::Mat fernsMaps = cv::Mat(sy, sx, CV_64FC(numFerns), cv::Scalar::all(0));
  cv::Mat fernsMaps = cv::Mat(sx, sy, CV_64FC(numFerns), cv::Scalar::all(0)); // to transpose the output

  // pointers
  double* HOGPtr = HOG.ptr<double>(0);
  double* fernsPtr = ferns.ptr<double>(0);
  double* fernsMapsPtr = fernsMaps.ptr<double>(0);

  // random ferns
  for (r=0; r<numFerns; r++){

    // fern features
    for (f=0; f<numFeats; f++){

      // temp. variable
      tmp2 = f*numFerns + r;
      tmp11 = (1 << f);

      // Note: the access format has changed due to the input cv::Mat
      // put the transpose flag to false (for multi-channel array)
      // old version: py = y + (int)fernsPtr[feat*6*numFerns + 0*numFerns + fern];
      // new version: py = y + (int)fernsPtr[0*numFeats*numFerns + feat*numFerns + fern];

      // point A coordinates
      ya = (int)fernsPtr[tmp2];
      xa = (int)fernsPtr[tmp1 + tmp2];
      ca = (int)fernsPtr[tmp5 + tmp2];

      // point B coordinates
      yb = (int)fernsPtr[tmp6 + tmp2];
      xb = (int)fernsPtr[tmp7 + tmp2];
      cb = (int)fernsPtr[tmp8 + tmp2];

      // convolution
      for (x=0; x<=mx; x++) {

        // temp. variables
        tmp9 = (xa+x)*nc + ca;
        tmp10 = (xb+x)*nc + cb;
        //tmp12 = x*numFerns + r;

        for (y=0; y<=my; y++) {

          // temp. variable
          tmp12 = y*numFerns + r; // to transpose the output

          // point values
          va = HOGPtr[(ya+y)*tmp3 + tmp9];
          vb = HOGPtr[(yb+y)*tmp3 + tmp10];

          // point comparison
          z = tmp11 & (0 - ((va - vb) > 0.01));

          // check
          if (z<0 || z>=numBins-1){ mexErrMsgTxt("Incorrect fern output"); }

          //------------------------------
          // MIKE: mejorar este if.. inicial value 1 o q no empiece vacio q da erroees
          //------------------------------

          // update fern output
          if (f==0){
            //fernsMapsPtr[y*tmp4 + tmp12] = z+1;
            fernsMapsPtr[x*tmp4 + tmp12] = z+1;  // to transpose the output
          }
          else{
            //fernsMapsPtr[y*tmp4 + tmp12] += z;
            fernsMapsPtr[x*tmp4 + tmp12] += z; // to transpose the output
          }

        }
      }
    }
  }

  // output
  return fernsMaps;
};

// histogram of oriented gradients (HOG) from integral image (II)
cv::Mat fun_II2HOG(cv::Mat II, int cellSize){

  // integral image size
  int sy = II.rows;  // height
  int sx = II.cols;  // width
  int nc = II.channels();  // num. channels

  // MIKE: mejorar... para que quede del mismo tamañ

  // new image size
  int ny = ceil((double)sy/cellSize)-1;
  int nx = ceil((double)sx/cellSize)-1;

  // HOG
  cv::Mat HOG = cv::Mat(ny, nx, CV_64FC(nc), cv::Scalar::all(0));

  // pointers
  double* IIPtr = II.ptr<double>(0);
  double* HOGPtr = HOG.ptr<double>(0);

  // variables
  double value;  // value
  int u1,v1,u2,v2;  // HOG coordinates

  // temp. variables
  int tmp1 = sx*nc;
  int tmp2 = nx*nc;
  double tmp3 = (double)cellSize*cellSize;
  int tmp4,tmp5,tmp6,tmp7,tmp8,tmp9,tmp10,tmp11,tmp12;

  // image
  for (int x=0; x<nx; x++) {

    // current region coordinates
    u1 = x*cellSize;
    u2 = min(u1+cellSize,sx-1);

    // temp. variables
    tmp4 = u1*nc;
    tmp5 = u2*nc;

    for (int y=0; y<ny; y++) {

      // current region coordinates
      v1 = y*cellSize;
      v2 = min(v1+cellSize,sy-1);

      // temp. variables
      tmp6 = v1*tmp1;
      tmp7 = v2*tmp1;
      tmp8 = tmp7 + tmp5;
      tmp9 = tmp6 + tmp4;
      tmp10 = tmp6 + tmp5;
      tmp11 = tmp7 + tmp4;
      tmp12 = y*tmp2 + x*nc;

      // channels
      for (int c=0; c<nc; c++){
        //value = *(IIPtr + bottom*sx*numChannels + right*numChannels + c) + *(IIPtr + top*sx*numChannels + left*numChannels + c) - *(IIPtr + top*sx*numChannels + right*numChannels + c) - *(IIPtr + bottom*sx*numChannels + left*numChannels + c);
        //(HOGPtr + y*nx*numChannels + x*numChannels + c) = value/((double)cellSize*cellSize);
        value = IIPtr[tmp8 + c] + IIPtr[tmp9 + c] - IIPtr[tmp10 + c] - IIPtr[tmp11 + c];
        HOGPtr[tmp12 + c] = value/tmp3;
      }
    }
  }

  // output
  return HOG;
};

// integral image (II) from histogram of oriented gradients (HOG)
cv::Mat fun_HOG2II(cv::Mat HOG){

  // HOG size
  int sy = HOG.rows;  // height
  int sx = HOG.cols;  // width
  int nc = HOG.channels();  // num. channels -gradient orientations-

  // integral image
  cv::Mat II = cv::Mat(sy, sx, CV_64FC(nc), cv::Scalar::all(0));

  // pointers
  double* IIPtr = II.ptr<double>(0);
  double* HOGPtr = HOG.ptr<double>(0);

  // variables
  double value,xo,yo,xoyo;  // accumulative II values

  // temp. variables
  int tmp1 = sx*nc;
  int tmp2,tmp3;

  // integral image computation -recursive computation-
  for (int c=0; c<nc; c++) {
    for (int x=0; x<sx; x++) {
      // temp. variable
      tmp2 = x*nc + c;
      tmp3 = (x-1)*nc + c;
      for (int y=0; y<sy; y++) {

        // HOG value at (x,y,c)
        value = HOGPtr[y*tmp1 + tmp2];

        // previous integral image values
        if (x>0) {
          if (y>0) {
            xo = IIPtr[y*tmp1 + tmp3];
            yo = IIPtr[(y-1)*tmp1 + tmp2];
            xoyo = IIPtr[(y-1)*tmp1 + tmp3];
          }
          else {
            xo = IIPtr[y*tmp1 + tmp3];
            yo = 0;
            xoyo = 0;
          }
        }
        else {
          if(y>0) {
            xo = 0;
            yo = IIPtr[(y-1)*tmp1 + tmp2];
            xoyo = 0;
          }
          else {
            xo = 0;
            yo = 0;
            xoyo = 0;
          }
        }

        // integral image value at (x,y,c)
        IIPtr[y*tmp1 + tmp2] = value - xoyo + xo + yo;

      }
    }
  }

  // output
  return II;
};

// histogram of oriented gradients (HOG)
cv::Mat fun_HOG(cv::Mat img, int numChannels, int cellSize){

  // image size
  int sy = img.rows;
  int sx = img.cols;

  // HOG size
  int hy = ceil((double)sy/cellSize);
  int hx = ceil((double)sx/cellSize);

  // angle increment
  double angInc = (double)PI/numChannels;

  // HOG
  cv::Mat HOG = cv::Mat(hy, hx, CV_64FC(numChannels), cv::Scalar::all(0));

  // pointers
  double* HOGPtr = HOG.ptr<double>(0);
  double* imgPtr = img.ptr<double>(0);

  // variables
  int binx,biny,binc;  // HOG bins
  double dy,dx,mag,ang;  // gradient derivatives, maginitude and orientation
  int nc = numChannels;  // num. gradient orientations -channels-

  // temp. variables
  int tmp1 = hx*nc;
  int tmp2 = sx-1;
  int tmp3 = sy-1;

  // scanning
  for (int x=1; x<tmp2; x++){
    for (int y=1; y<tmp3; y++){

      // derivatives
      dy = imgPtr[(y+1)*sx + x] - imgPtr[(y-1)*sx + x];
      dx = imgPtr[y*sx + (x+1)] - imgPtr[y*sx + (x-1)];
      //dy = *(imgPtr + (y+1)*sx + (x-1)) + *(imgPtr + (y+1)*sx + (x+0)) + *(imgPtr + (y+1)*sx + (x+1)) - *(imgPtr + (y-1)*sx + (x-1)) - *(imgPtr + (y-1)*sx + (x+0)) - *(imgPtr + (y-1)*sx + (x+1));
      //dx = *(imgPtr + (y-0)*sx + (x+1)) + *(imgPtr + (y+0)*sx + (x+1)) + *(imgPtr + (y+1)*sx + (x+1)) - *(imgPtr + (y-1)*sx + (x-1)) - *(imgPtr + (y+0)*sx + (x-1)) - *(imgPtr + (y+1)*sx + (x-1));

      // gradient magnitude
      //mag = fabs(dx) + fabs(dy);
      mag = sqrt(dx*dx + dy*dy);
      //mag = dx*dx + dy*dy;

      // reject small gradients
      if (mag<0.0001){ continue; }

      // gradient angle
      ang = atan2(dy,dx);

      // 180 degrees gradient format (0-180) "unsigned format"
      if (ang<0){ ang = fabs(ang + PI); }

      // orientation channel bin
      binc = floor(ang/angInc);
      if (binc==nc){ binc = 0; }

      // check
      //if (binc<0 || binc>nc){ mexErrMsgTxt("Incorrect gradient orientation bin"); }
      // spatial bins
      binx = floor(double(x)/cellSize);
      biny = floor(double(y)/cellSize);

      // check
      //if (binx<0 || binx>=hx || biny<0 || biny>=hy){ mexErrMsgTxt("Incorrect gradient spatial bin"); }

      // HOG computation
      HOGPtr[biny*tmp1 + binx*nc + binc] +=  mag;

    }
  }

  // output
  return HOG;
};
