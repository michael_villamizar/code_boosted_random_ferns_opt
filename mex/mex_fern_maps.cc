#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include "mexopencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include "detector_functions.h"
#include "detector_functions.cpp"

using namespace std;

// main function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  // check
  if(nrhs!=3) { mexErrMsgTxt("Three inputs required: 1. image 2. fern data 3. fern size"); }
  if(nlhs!=1) { mexErrMsgTxt("One output required: 1. fern maps"); }

  // variables
  cv::Mat img = MxArray(prhs[0]).toMat();  // input image
  cv::Mat ferns = MxArray(prhs[1]).toMat(CV_64F, false);  //ferns data : binary comparison tests
  int fernSize = MxArray(prhs[2]).toInt();  // fern size

  // compute ferns maps
  cv::Mat maps = fun_ferns_maps(img, ferns, fernSize);

  // output
  plhs[0] = MxArray(maps, mxUNKNOWN_CLASS, false);
}
