%% parameters
% This function defines the program parameters. They can be modified
% -respectively- in the experiment function in order to measure the impact 
% of these parameters over the detection performance of BRFs.
function output = fun_parameters()

% experiments
prmsExp = fun_experiments();

% random ferns
fernSize = prmsExp.fernSize;                                                % fern size                  
numFerns = prmsExp.numFerns;                                                % num. random ferns                                          
numFeats = prmsExp.numFeats;                                                % num. fern features per fern                              
ferns = struct('numFeats',numFeats,'numFerns',numFerns,'fernSize',fernSize);

% image data
cellSize = 6;                                                               % HOG cell -pixels-
imgSpace = prmsExp.imgSpace;                                                % image {HOG,GRAY,RGB}
numOriBins = 4;                                                             % num. gradient orientation bins  
imgData = struct('imgSpace',imgSpace,'numOriBins',numOriBins,'cellSize',cellSize);

% train data
posImgFmt = '.pgm';                                                         % positive image format
negImgFmt = '.pgm';                                                         % negative image format
numPosImgs = 500;                                                           % num. positive images
numNegImgs = 500;                                                           % num. negative images
posImgPath = './images/datasets/uiuc_cars/train_images/positives/';         % positive image files
negImgPath = './images/datasets/uiuc_cars/train_images/negatives/';         % negative image files
posAnnPath = '';                                                            % positive annotations files
negAnnPath = '';                                                            % negative annotations files
train = struct('numPosImgs',numPosImgs,'numNegImgs',numNegImgs,'posImgPath',posImgPath,'negImgPath',negImgPath,...
    'posImgFmt',posImgFmt,'negImgFmt',negImgFmt,'posAnnPath',posAnnPath,'negAnnPath',negAnnPath);

% test data
posImgFmt = '.pgm';                                                         % positive image format
negImgFmt = '.pgm';                                                         % negative image format
numPosImgs = 108;                                                            % num. positive images
numNegImgs = 0;                                                             % num. negative images
posImgPath = './images/datasets/uiuc_cars/test_images/positives/';          % positive image files
negImgPath = './images/datasets/uiuc_cars/test_images/negatives/';          % negative image files
posAnnPath = '';                                                            % positive annotations files
negAnnPath = '';                                                            % negative annotations files
test = struct('numPosImgs',numPosImgs,'numNegImgs',numNegImgs,'posImgPath',posImgPath,'negImgPath',negImgPath,...
    'posImgFmt',posImgFmt,'negImgFmt',negImgFmt,'posAnnPath',posAnnPath,'negAnnPath',negAnnPath);

% classifier
eps = 0.001;                                                                % epsilon (BRFs)
omega = 0.01;                                                               % outlier threshold (BRFs)
numWCs = prmsExp.numWCs;                                                    % num. weak classifiers
objSize = prmsExp.size;                                                     % object size
numMaxPWCs = 4000; % 2000                                                          % num. max. potential weak classifiers (PWCs)
classifier = struct('numWCs',numWCs,'objSize',objSize,'eps',eps,'omega',omega,'numMaxPWCs',numMaxPWCs);

% runtime
detThr = 0.05;                                                              % detection threshold                                                  
imgExt = 30;                                                                % image file extension
intThr = 0.3;                                                               % intersection area -overlapping- threshold    
detThick = 5;                                                               % detection rectangle thickness
imgHeight = 320;                                                            % image height    
minImgSize = 10;                                                            % min. image size        
numMaxDets = 1000;                                                          % num. max. detections
minCellSize = 3;                                                            % min. cell size        
maxCellSize = 50;                                                           % max. cell size
numImgLevels = 5;                                                           % num. image levels
numMaxOutputs = 20;                                                         % num. maximum of detection outputs    
runtime = struct('numImgLevels',numImgLevels,'minCellSize',minCellSize,'maxCellSize',maxCellSize,'detThr',detThr,...
    'minImgSize',minImgSize,'numMaxOutputs',numMaxOutputs,'imgHeight',imgHeight,'intThr',intThr,'imgExt',imgExt,...
    'detThick',detThick,'numMaxDets',numMaxDets);
 
% colors
posColor = [0,1,0];                                                         % positive color
negColor = [1,0,0];                                                         % negative color
annColor = [0.1,0.1,0.1];                                                   % annotation colot
colors = struct('posColor',posColor,'negColor',negColor,'annColor',annColor);
 
% visualization
gridSize = 100;                                                             % grid size
fontSize = 18;                                                              % font width
lineWidth = 4;                                                              % line width
markerSize = 16;                                                            % marker size
visualization = struct('markerSize',markerSize,'lineWidth',lineWidth,'fontSize',fontSize,'gridSize',gridSize);

% program parameters
prms.test = test;
prms.ferns = ferns;
prms.train = train;
prms.colors = colors;
prms.imgData = imgData;
prms.runtime = runtime;
prms.classifier = classifier;
prms.visualization = visualization;

% output
output = prms;
end
