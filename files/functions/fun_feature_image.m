%% feature image
% This function processes the input image -img- to work with particular 
% features: pixel intensities (GRAY,RGB) or oriented gradients (HOG). The 
% image is also scaled to the object size defined to train the classifier.
function output = fun_feature_image(img,imgSize)
if (nargin~=2), fun_messages('incorrect input parameters','error'); end
    
% parameters
prms = fun_parameters();  % program parameters
imgSpace = prms.imgData.imgSpace;  % image feature space

% feature image
switch (imgSpace)
    case 'HOG'
        % histogram of oriented gradients (HOG)
        img = fun_hog(img,imgSize);
     case 'GRAY'
        % gray-scaled image
        img = imresize(img,imgSize(1:2),'bilinear');
        img = fun_image_color(img,'GRAY');
    case 'RGB'
        % color -RGB- image
        img = imresize(img,imgSize(1:2),'bilinear');
        img = fun_image_color(img,'RGB');
    otherwise
        fun_messages('incorrect image feature space','error');
end

% output
output = img;
end
