%% potential weak classifiers (PWCs)
% This function computes the pool of weak classifier candidates, which are 
% then chosen by the learning algorithm to compute the object classifier. 
% Each potential weak classifier corresponds to a fern computed at 
% particular image location (y,x).
function output = fun_potential_weak_classifiers()

% parameters
prms = fun_parameters();  % program parameters                                            
objSize = prms.classifier.objSize;  % object size                             
numFerns = prms.ferns.numFerns;  % num. shared random ferns                           
fernSize = prms.ferns.fernSize;  % fern size -spatial size-                          
numMaxPWCs = prms.classifier.numMaxPWCs;  % num. max. PWCs

% variables
cont = 0;   % counter                        

% image limits
ly = objSize(1) - fernSize + 1;
lx = objSize(2) - fernSize + 1;

% vectors
vy = 1:ly;
vx = 1:lx;

% num. potential weak classifiers
numPWCs = size(vy,2)*size(vx,2)*numFerns;

% allocate
PWCs = zeros(numPWCs,3);

% create potential weak classifier
for iterFern = 1:numFerns
    for iterY = 1:ly
        for iterX = 1:lx
            % update counter
            cont = cont + 1;
            % weak classifier data: location and fern index
            PWCs(cont,:) = [iterY,iterX,iterFern];
        end
    end
end

% check
if(cont~=numPWCs),fun_messages('incorrect number of potential weak classifiers','error'); end

% reduce num. PWCs
if (cont>numMaxPWCs)
    % message
    fun_messages(sprintf('reducing the num. of PWCS from %d to %d',numPWCs,numMaxPWCs),'warning');
    % random permutation -> random indexes
    indxs = randperm(cont);
    PWCs = PWCs(indxs(1:numMaxPWCs),:);
end

% message
fun_messages(sprintf('num. potential weak classifiers: %d',numPWCs),'information');

% output
output = PWCs;
end
