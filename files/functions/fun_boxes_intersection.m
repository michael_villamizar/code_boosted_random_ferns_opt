%% boxes intersection
% This function measure the overlapping -intersection- betweem two sets of
% boxes.
function output = fun_boxes_intersection(boxesA,boxesB,threshold)

% num. boxes
numBoxesA = size(boxesA,1);
numBoxesB = size(boxesB,1);

% variables
matValues = zeros(numBoxesA,numBoxesB);

% intersection measure
for iterA = 1:numBoxesA
    
    % box A
    boxA = boxesA(iterA,:);
    
    for iterB = 1:numBoxesB
        
        % box B
        boxB = boxesB(iterB,:);
        
        % intersection area
        x1 = max(boxA(1),boxB(1));
        y1 = max(boxA(2),boxB(2));
        x2 = min(boxA(3),boxB(3));
        y2 = min(boxA(4),boxB(4));
        intArea = (x2-x1)*(y2-y1);
        
        % union area
        areaA = (boxA(3)-boxA(1))*(boxA(4)-boxA(2));
        areaB = (boxB(3)-boxB(1))*(boxB(4)-boxB(2));
        uniArea = areaA + areaB - intArea;
        
        % measure
        value = intArea/uniArea;
        
        % check : no intersection
        if ((x2-x1<0) || (y2-y1<0)), value = 0; end
        
        % check
        %if (value<threshold),value = 0; end
        
        % save value
        matValues(iterA,iterB) = value;
        
    end
end

% sort values
[values,indexes] = sort(matValues(:));

% classification
matInd = zeros(numBoxesA,numBoxesB);
for iter = size(values,1):-1:1
    
    % current index
    [i,j] = ind2sub(size(matValues),indexes(iter));
    
    % matching
    if (sum(matInd(:,j),1)==0) && (sum(matInd(i,:),2)==0) && matValues(i,j)>=threshold
        matInd(i,j)= 1;
    end
    
end

% intersection data
data.indexes = matInd;
data.values  = matValues;

% output
output = data;
end


